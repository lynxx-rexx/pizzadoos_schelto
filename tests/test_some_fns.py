from src.some_fns import add_fish_to_aquarium
 
def test_add_fish_to_aquarium_success():
   actual = add_fish_to_aquarium(fish_list=["shark", "tuna"])
   expected = {"tank_a": ["shark", "tuna"]}
   assert actual == expected

def test_add_fish_to_aquarium_succes():
   actual = add_fish_to_aquarium(fish_list=["salmon", "tuna"])
   expected = {"tank_a": ["shark", "tuna"]}
   assert actual != expected 